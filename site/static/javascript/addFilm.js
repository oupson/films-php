window.addEventListener('load', () => {
    const selectRealisateurFilm = document.getElementById("input_realisateur_film");
    const selectGenreFilm = document.getElementById("input_genre_film");
    const imageUrl = document.getElementById("input_url_image_film");
    const imagePreview = document.getElementById("img_preview");

    // region MODAL
    const modalReal = document.getElementById("modal_real");
    const submitRealButton = document.getElementById("submit_real_modal");
    const nameRealInput = document.getElementById("input_real_modal");
    const descriptionRealInput = document.getElementById("input_real_modal");
    const urlImageRealInput = document.getElementById("input_url_real_modal");
    const realContainer = document.getElementById("container_add_real");
    // endregion


    // region GENRE
    const modalGenre = document.getElementById("modal_genre");
    const submitGenreButton = document.getElementById("submit_genre_modal");
    const nameGenreInput = document.getElementById("input_genre_modal");
    const descriptionGenreInput = document.getElementById("input_genre_modal");
    const genreContainer = document.getElementById("container_add_genre");
    // endregion

    imagePreview.style.background = "no-repeat center/contain url(\"" + imageUrl.value + "\")";

    imageUrl.onchange = () => {
        imagePreview.style.background = "no-repeat center/contain url(\"" + imageUrl.value + "\")";
    }

    selectRealisateurFilm.addEventListener('change', (e) => {
        console.log(e.target.selectedOptions[0].id);
        if (e.target.selectedOptions[0].id == "add_a_real") {
            modalReal.style.display = "block";
        }
    });

    selectGenreFilm.addEventListener('change', (e) => {
        if (e.target.selectedOptions[0].id == "add_a_genre") {
            modalGenre.style.display = "block";
        }
    });

    window.onclick = (event) => {
        if (event.target == modalReal) {
            if (realContainer.firstElementChild.classList.contains("error-message")) {
                realContainer.removeChild(realContainer.firstElementChild);
            }
            modalReal.style.display = "none";
        } else if (event.target == modalGenre) {
            if (genreContainer.firstElementChild.classList.contains("error-message")) {
                genreContainer.removeChild(genreContainer.firstElementChild);
            }
            modalGenre.style.display = "none";
        }
    };


    const spanReal = document.getElementById("close_real_modal");

    spanReal.onclick = () => {
        modalReal.style.display = "none";
    };


    submitRealButton.onclick = () => {
        const name = nameRealInput.value;
        const description = descriptionRealInput.value;
        const urlImage = urlImageRealInput.value;

        fetch('api/createReal.php', {
            method: 'POST',
            body: new URLSearchParams({
                'nameReal': name,
                'imageReal': urlImage,
                'descReal': description
            })
        })
            .then(r => {
                if (r.status == 200) {
                    return r.json();
                } else {
                    return new Promise(async (resolve, reject) => {
                        reject({
                            "json": await r.json(),
                            "status": r.status,
                        });
                    });
                }
            })
            .then(res => {
                const option = document.createElement("option");
                option.value = res.realId;
                option.innerHTML = res.nameReal;

                selectRealisateurFilm.insertBefore(option, selectRealisateurFilm.lastElementChild);

                selectRealisateurFilm.value = res.realId;

                if (realContainer.firstElementChild.classList.contains("error-message")) {
                    realContainer.removeChild(realContainer.firstElementChild);
                }
                modalReal.style.display = "none";
            }).catch(e => {
                if (e.json != undefined) {
                    const div = document.createElement("div");
                    div.classList.add("error-message");

                    const msg = document.createElement("p");
                    msg.innerHTML = e.json.error.error_message;
                    div.appendChild(msg);

                    realContainer.insertBefore(div, realContainer.firstElementChild);
                }
            });
    };

    const spanGenre = document.getElementById("close_genre_modal");

    spanGenre.onclick = () => {
        modalGenre.style.display = "none";
    };


    submitGenreButton.onclick = () => {
        const name = nameGenreInput.value;
        const description = descriptionGenreInput.value;

        fetch('api/createGenre.php', {
            method: 'POST',
            body: new URLSearchParams({
                'nameGenre': name,
                'descGenre': description
            })
        })
            .then(r => {
                if (r.status == 200) {
                    return r.json();
                } else {
                    return new Promise(async (resolve, reject) => {
                        reject({
                            "json": await r.json(),
                            "status": r.status,
                        });
                    });
                }
            })
            .then(res => {
                const option = document.createElement("option");
                option.value = res.genreId;
                option.innerHTML = res.nameGenre;

                selectGenreFilm.insertBefore(option, selectGenreFilm.lastElementChild);

                selectGenreFilm.value = res.genreId;

                if (genreContainer.firstElementChild.classList.contains("error-message")) {
                    genreContainer.removeChild(genreContainer.firstElementChild);
                }
                modalGenre.style.display = "none";
            }).catch(e => {
                if (e.json != undefined) {
                    const div = document.createElement("div");
                    div.classList.add("error-message");

                    const msg = document.createElement("p");
                    msg.innerHTML = e.json.error.error_message;
                    div.appendChild(msg);

                    genreContainer.insertBefore(div, genreContainer.firstElementChild);
                }
            });
    };
});