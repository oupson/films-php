<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Collection de films</title>
    <link rel="stylesheet" href="./static/style/main.css" />
    <link rel="stylesheet" href="style.css">
</head>
<?php
require_once("imports.php");
require_once("fonctions.php");

use \FilmPHP\Database\Connexion;

$file_db = new Connexion();
$title = "Collection de films";
?>

<body>
    <?php include __ROOT__ . "/private/parts/header.php"; ?>
    <main class="article">
        <aside>
            <h2>Criteres de recherche</h2>

            <nav class="filtres">
                <form class="filtres" method="get">
                    <nav class="ordreTrie">
                        <h4>Ordre de tri :</h4>
                        <!-- le php ici sert a garder les criteres de recherche apres avoir valider le formulaire-->
                        <input type="radio" id="triAsc" name="desc" value="ascendant" <?php if (isset($_GET['desc'])) {
                                                                                            if ($_GET['desc'] == "ascendant") {
                                                                                                echo "checked";
                                                                                            }
                                                                                        } else {
                                                                                            echo "checked";
                                                                                        } ?>>
                        <label for="triAsc">Ordre ascendant</label>
                        <input type="radio" id="triDesc" name="desc" value="descendant" <?php if (isset($_GET['desc'])) {
                                                                                            if ($_GET['desc'] == "descendant") {
                                                                                                echo "checked";
                                                                                            }
                                                                                        } ?>>
                        <label for="triDesc">Ordre descendant</label>
                    </nav>
                    <nav>
                        <h4>Trier par :</h4>
                        <select name="TrierPar" id="tri-select">
                            <option value="titreFilm" <?php if (isset($_GET['TrierPar'])) {
                                                            if ($_GET['TrierPar'] == "titreFilm") {
                                                                echo "selected";
                                                            } else {
                                                                echo "selected";
                                                            }
                                                        } ?>>Titre</option>
                            <option value="nomGenre" <?php if (isset($_GET['TrierPar'])) {
                                                            if ($_GET['TrierPar'] == "nomGenre") {
                                                                echo "selected";
                                                            }
                                                        } ?>>Genre</option>
                            <option value="anneeFilm" <?php if (isset($_GET['TrierPar'])) {
                                                            if ($_GET['TrierPar'] == "anneeFilm") {
                                                                echo "selected";
                                                            }
                                                        } ?>>Date realisation</option>
                            <option value="nomRealisateur" <?php if (isset($_GET['TrierPar'])) {
                                                                if ($_GET['TrierPar'] == "nomRealisateur") {
                                                                    echo "selected";
                                                                }
                                                            } ?>>Realisateur</option>
                        </select>
                    </nav>

                    <fieldset class="genres">
                        <legend>Selectionner les genres</legend>
                        <?php
                        affichageGenres($file_db);
                        ?>
                    </nav>
                    <input type='submit' value='valider'>
                </form>

            </nav>

        </aside>
        <article>
            <h2>Liste de films</h2>
            <?php
            $rad = $_GET['desc'] ?? 'asc';
            $tri = $_GET['TrierPar'] ?? 'titreFilm';
            $gen = $_GET['genre'] ?? NULL;
            affichageFilms($file_db, $rad, $tri, $gen);
            $file_db = NULL
            ?>
        </article>

    </main>
    <?php include __ROOT__ . "/private/parts/footer.php"; ?>
</body>

</html>