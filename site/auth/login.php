<?php
require_once "../imports.php";
$title = "Se connecter";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="/static/style/main.css" />
    <link rel="stylesheet" href="/static/style/auth.css" />
</head>

<body>
    <?php include __ROOT__ . "/private/parts/header.php"; ?>
    <div class="centered-prompt article">
        <?php

        use FilmPHP\Database\Connexion;

        $conn = new Connexion();

        function sessionExist(PDO $conn, string $guid): bool
        {
            $st = $conn->prepare("SELECT 'foo' FROM SESSION WHERE tokenSession = :guid");
            $st->bindParam(":guid", $guid, PDO::PARAM_STR);
            $st->execute();
            $res = $st->fetch();
            return $res != false && $res != null; // fetch can be null or false or something
        }

        $utilisateur = $conn->getUserFromSession(isset($_COOKIE["session"]) ? $_COOKIE["session"] : null);
        if ($utilisateur != null) {
            echo "<div class=\"info\"><p>Vous êtes deja connecté</p></div>\n";

            if (isset($_GET["redirect"])) { // TODO CHECK IF IN SERVER
                header("Location: " . $_GET["redirect"]);
            }
        } else {
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                if (isset($_POST["username"]) && isset($_POST["password"])) {

                    $username = $_POST["username"];
                    $password = hash('sha512', $_POST["password"], true);

                    $conn = new Connexion();

                    $st = $conn->prepare("SELECT idUtilisateur, nomUtilisateur FROM UTILISATEUR WHERE nomUtilisateur = :username AND mdpUtilisateur = :password");
                    $st->bindParam(":username", $username, PDO::PARAM_STR);
                    $st->bindParam(":password", $password);
                    $st->execute();

                    $res = $st->fetch();

                    if ($res) {
                        do {
                            $uuid = GUIDv4();
                        } while (sessionExist($conn, $uuid));

                        $st = $conn->prepare("INSERT INTO SESSION (tokenSession, idUtilisateur) VALUES(:session, :idUtilisateur)");
                        $st->bindParam(":session", $uuid, PDO::PARAM_STR);
                        $st->bindParam(":idUtilisateur", $res["idUtilisateur"], PDO::PARAM_INT);
                        $st->execute();

                        setcookie("session", $uuid, 0, "/");

                        if (isset($_GET["redirect"])) { // TODO CHECK IF IN SERVER
                            header("Location: " . $_GET["redirect"]);
                        } else {
                            echo "<div class=\"info\"><p>Bienvenue, " . $res["nomUtilisateur"] . " !</p></div>\n";
                        }
                    } else {
                        echo "<div class=\"error-message\"><p>Compte ou mot de passe invalide</p></div>\n";
                    }
                } else {
                    http_response_code(400);
                    echo "<div class=\"error-message\"><p>Il manque des informations</p></div>\n";
                }
            } else {
                echo file_get_contents(__STATIC__ . "/html/login_form.html") . "\n";
            }
        }
        ?>
    </div>
    <?php include __ROOT__ . "/private/parts/footer.php"; ?>
</body>

</html>