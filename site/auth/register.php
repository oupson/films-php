<?php
require_once "../imports.php";
$title = "Créer un compte";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="/static/style/main.css" />
    <link rel="stylesheet" href="/static/style/auth.css" />
</head>

<body>
    <?php include __ROOT__ . "/private/parts/header.php"; ?>
    <div class="centered-prompt article">
        <?php
        require_once "../imports.php";

        use FilmPHP\Database\Connexion;

        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
            if (isset($_POST["username"]) && isset($_POST["password"]) && $email) {
                $username = $_POST["username"];
                $password = hash('sha512', $_POST["password"], true);

                $conn = new Connexion();

                $st = $conn->prepare("SELECT * FROM UTILISATEUR WHERE nomUtilisateur = :username");
                $st->bindParam(":username", $username, PDO::PARAM_STR);
                $st->execute();
                $res = $st->fetch();

                if ($res == false) {
                    try {
                        $st = $conn->prepare("INSERT INTO UTILISATEUR (nomUtilisateur, roleUtilisateur, mdpUtilisateur, emailUtilisateur) VALUES(:username, 0, :password, :email)");
                        $st->bindParam(":username", $username);
                        $st->bindParam(":password", $password);
                        $st->bindParam(":email", $email);
                        $st->execute();

                        header("Location: login.php");
                    } catch (\Exception $e) {
                        http_response_code(500);
                        echo "<div class=\"error-message\"><p>Une erreur a été rencontrée</p></div>";
                    }
                } else {
                    echo "<div class=\"error-message\"><p>Le compte existe deja</p></div>";
                }
            } else {
                echo "<div class=\"error-message\"><p>Il manque des paramètres</p></div>";
            }
        }
        ?>
        <form method="post">
            <div>
                <label for="username_input">Nom d'utilisateur :</label>
                <input id="username_input" name="username" placeholder="totodu45" required="true" />
            </div>
            <div>
                <label for="email_input">Adresse Email :</label>
                <input id="email_input" name="email" placeholder="monemail@monsite.fr" required="true" type="email" />
            </div>
            <div>
                <label for="password_input">Mot de passe :</label>
                <input id="password_input" name="password" placeholder="Mon MDP le plus sur" type="password" required="true" />
            </div>
            <div>
                <a href="login.php"> <input type="button" value="Se connecter" /></a>
                <input type="submit" value="Créer un compte" />
            </div>
        </form>
    </div>
    <?php include __ROOT__ . "/private/parts/footer.php"; ?>
</body>

</html>