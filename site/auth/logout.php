<?php
require_once "../imports.php";

use FilmPHP\Database\Connexion;

$title = "Se déconnecter";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="/static/style/main.css" />
</head>

<body>
    <?php include __ROOT__ . "/private/parts/header.php"; ?>
    <div class="centered-prompt article">
        <?php
        if (isset($_COOKIE["session"])) {
            if ($_GET["logout"] == "yes") {
                $conn = new Connexion();

                $st = $conn->prepare("DELETE FROM SESSION WHERE tokenSession = :token");
                $st->bindParam(":token", $_COOKIE["session"]);
                $st->execute();

                setcookie("session", "", time() - 3600, "/");
                if (isset($_GET["redirect"])) { // TODO CHECK IF IN SERVER
                    header("Location: " . $_GET["redirect"]);
                } else {
                    header("Location: /");
                }
            } else {
                echo "<a href=\"logout.php?logout=yes\"><input type=\"button\" value=\"Se déconnecter\" /></a>";
            }
        } else {
            echo "<div class=\"error-message\"><p>Vous n'êtes pas connectés</p></div>";
        }
        ?>
    </div>
    <?php include __ROOT__ . "/private/parts/footer.php"; ?>
</body>

</html>