<?php

declare(strict_types=1);

require_once "imports.php";

use FilmPHP\Database\Connexion;

$title = "Supprimer document";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="./static/style/main.css" />
</head>

<body>
    <?php include __ROOT__ . "/private/parts/header.php"; ?>
    <div class="article">
        <?php

        if ($_SERVER['REQUEST_METHOD'] == "GET") {
            $conn = new Connexion();
            if (isLoggedIn($_COOKIE, $conn)) {
                if (isset($_GET["filmId"])) {
                    if (!$conn->filmExist($_GET["filmId"])) {
                        http_response_code(500);
                        echo "<div class=\"error-message\"><p>Le film n'existe pas</p></div>";
                    } else {
                        $rowId = $conn->deleteFilm($_GET["filmId"]);
                        echo "<div class=\"info-message\"><p>Film supprimé</p></div>";
                    }
                } else {
                    http_response_code(500);
                    echo "<div class=\"error-message\"><p>Il manque des options</p></div>";
                }
            } else {
                http_response_code(403);
                echo "<div class=\"error-message\"><p>Vous n'avez pas le droit de faire ça</p></div>";
            }
        } else {
            http_response_code(500);
            echo "<div class=\"error-message\"><p>La méthode est incorrecte</p></div>";
        }

        ?>
    </div>
    <?php include __ROOT__ . "/private/parts/footer.php"; ?>
</body>

</html>