<?php

namespace FilmPHP\Database {
    class Connexion extends \PDO
    {
        public static string $conn = 'sqlite:' . __ROOT__ . '/film.db';

        public function __construct()
        {
            parent::__construct($this::$conn);
            $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);
        }

        function getUserFromSession(?string $session)
        {
            if ($session == null) {
                return null;
            }

            $st = $this->prepare("SELECT UTILISATEUR.* FROM SESSION NATURAL JOIN UTILISATEUR WHERE tokenSession = :session");
            $st->bindParam(":session", $session);
            $st->execute();

            return $st->fetch();
        }

        function realExist(string $real): bool
        {
            $st = $this->prepare("SELECT 'foo' FROM REALISATEUR WHERE nomRealisateur = :name");
            $st->bindParam(":name", $real);
            $st->execute();

            return $st->fetch() != null;
        }

        function insertReal(string $nameReal, ?string $imageReal, ?string $descReal): int
        {
            $st = $this->prepare("INSERT INTO REALISATEUR (nomRealisateur, imageRealisateur, descriptionRealisateur) VALUES(:name, :image, :desc)");
            $st->bindParam(":name", $nameReal);
            $st->bindParam(":image", $imageReal);
            $st->bindParam(":desc", $descReal);

            $st->execute();
            return $this->lastInsertId();
        }

        function genreExist(string $genre): bool
        {
            $st = $this->prepare("SELECT 'foo' FROM GENRE WHERE nomGenre = :name");
            $st->bindParam(":name", $genre);
            $st->execute();

            return $st->fetch() != null;
        }

        function insertGenre(string $nameGenre, ?string $descGenre): int
        {
            $st = $this->prepare("INSERT INTO GENRE (nomGenre, descriptionGenre) VALUES(:name, :desc)");
            $st->bindParam(":name", $nameGenre);
            $st->bindParam(":desc", $descGenre);

            $st->execute();
            return $this->lastInsertId();
        }

        function filmExist(string $film): bool
        {
            $st = $this->prepare("SELECT 'foo' FROM FILM WHERE idFilm = :filmId");
            $st->bindParam(":filmId", $film);
            $st->execute();

            return $st->fetch() != null;
        }

        function deleteFilm(string $filmId)
        {
            $st = $this->prepare("DELETE FROM FILM WHERE idFilm = :filmId");
            $st->bindParam(":filmId", $filmId);

            $st->execute();
        }
    }
}
