<header>
    <h1><?php echo $title ?></h1>
    <nav>
        <a href="/">Acceuil</a>
        <a href="/createFilm.php">Ajouter un film</a>
        <?php
        require_once __ROOT__ . "/imports.php";
        if (isLoggedIn($_COOKIE)) {
            echo "<a href=\"/auth/logout.php\">Se deconnecter</a>\n";
        } else {
            echo "<a href=\"/auth/login.php\">Se connecter</a>\n";
        }
        ?>
    </nav>
</header>