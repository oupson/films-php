<?php
require_once __ROOT__."/imports.php";
function isLoggedIn(array $tab, \FilmPHP\Database\Connexion $conn = null): bool
{
    if (isset($tab["session"])) {
        $db = $conn ?? new \FilmPHP\Database\Connexion();
        return $db->getUserFromSession($tab["session"]) != null;
    } else {
        return false;
    }
}
