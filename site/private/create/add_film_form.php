<script src="./static/javascript/addFilm.js"></script>
<form method="post">
    <fieldset>
        <legend>Ajouter un film</legend>
        <div>
            <label for="input_name_film">Nom du film : </label>
            <input type="text" id="input_name_film" name="name_film" required />
        </div>
        <div>
            <label for="input_description_film">Description du film : </label>
            <input type="text" id="input_description_film" name="description_film" />
        </div>
        <div>
            <label for="input_annee_film">Année de sortie du film : </label>
            <input type="number" min="1888" max="<?php echo date("Y") ?>" step="1" id="input_annee_film" name="annee_film" required />
        </div>
        <div>
            <label for="input_url_image_film">Url de l'image du film : </label>
            <input type="url" id="input_url_image_film" name="url_image_film" />
        </div>
        <div>
            <label for="input_realisateur_film">Réalisateur du film : </label>
            <select name="id_realisateur_film" id="input_realisateur_film">
                <?php
                foreach ($conn->query("SELECT idRealisateur, nomRealisateur FROM REALISATEUR ORDER BY nomRealisateur") as $genre) {
                    echo "<option value=\"" . $genre["idRealisateur"] . "\">" . $genre["nomRealisateur"] . "</option>\n";
                }
                ?>
                <option value="" id="add_a_real">Ajouter un réalisateur</option>
            </select>
        </div>
        <div>
            <label for="input_genre_film">Genre du film : </label>
            <select name="id_genre_film" id="input_genre_film">
                <?php
                foreach ($conn->query("SELECT idGenre, nomGenre FROM GENRE ORDER BY nomGenre") as $genre) {
                    echo "<option value=\"" . $genre["idGenre"] . "\">" . $genre["nomGenre"] . "</option>\n";
                }
                ?>
                <option value="" id="add_a_genre">Ajouter un genre</option>
            </select>
        </div>
        <div></div>
        <div>
            <input type="submit" value="Ajouter le film"/>
        </div>
    </fieldset>
</form>

<fieldset>
    <legend>Image du film</legend>
    <div class="img-preview-container" id="img_preview"></div>
</fieldset>


<div id="modal_real" class="modal">
    <div class="modal-content">
        <span id="close_real_modal" class="close">&times;</span>
        <h1>Ajouter un réalisateur</h1>
        <div id="container_add_real">
            <div>
                <label for="input_real_modal">Nom du réalisateur</label>
                <input type="text" id="input_real_modal" />
            </div>
            <div>
                <label for="input_description_real_modal">Description du réalisateur</label>
                <input type="text" id="input_description_real_modal" />
            </div>
            <div>
                <label for="input_url_real_modal">URL de l'image du réalisateur</label>
                <input type="url" id="input_url_real_modal" />
            </div>
            <div>
                <input type="button" id="submit_real_modal" value="Ajouter le réalisateur" />
            </div>
        </div>
    </div>
</div>

<div id="modal_genre" class="modal">
    <div class="modal-content">
        <span id="close_genre_modal" class="close">&times;</span>
        <h1>Ajouter un genre</h1>
        <div id="container_add_genre">
            <div>
                <label for="input_genre_modal">Nom du genre</label>
                <input type="text" id="input_genre_modal" />
            </div>
            <div>
                <label for="input_description_genre_modal">Description du genre</label>
                <input type="text" id="input_description_genre_modal" />
            </div>
            <div>
                <input type="button" id="submit_genre_modal" value="Ajouter le genre" />
            </div>
        </div>
    </div>
</div>