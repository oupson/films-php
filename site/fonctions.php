<?php

function filtre_tri(string $tri): string //retourne le nom du tri (par genre, par titre etc) si un des genres etait choisi. Les films seront tries par titre par defaut si l'ordre ecrit dans get ne correspond a un des tris
{
    switch ($tri) {
        case "nomGenre":
            return $tri;
            break;
        case "anneeFilm":
            return $tri;
            break;
        case "titreFilm":
            return $tri;
            break;
        case "nomRealisateur":
            return $tri;
            break;
        default:
            return "titreFilm";
            break;
    }
}

//fait l'affichage des films en fonction des genres choisis, avec un ordre decroissant/croissant et par annee de realisation, titre etc
function affichageFilms(PDO $conn, ?string $desc, ?string $tri, ?array $gen): void
{
    $requete = requetesAffichageFilms($conn, $desc, $tri, $gen);
    foreach ($requete as $m) { //affichage de tous les films sous forme d'un bloc
        echo "<nav class='film'>
    <h2>" . $m['titreFilm'] . " - " . $m['nomGenre'] . "</h2>
    <h3>" . $m['nomRealisateur'] . "</h3>
    <p>" . $m['anneeFilm'] . "</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
      Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
      Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    <nav class='imageFilm'>
      <img src=" . $m['imageFilm'] . " alt='imageLol'>
    </nav>
    <a href='deleteFilm.php?filmId=" . $m["idFilm"] . "'><input type='button' class='styled' value='Supprimer ce film'</input></a>
  </nav>";
    }
}

function requetesAffichageFilms(PDO $conn, ?string $desc, ?string $tri, ?array $gen)
{
    $requete = 'SELECT * from film inner join realisateur ON FILM.idRealisateurFilm = realisateur.idRealisateur
  inner join genre ON FILM.idGenreFilm = genre.idGenre ';
    $tri = filtre_tri($tri);
    if ($desc == 'descendant') {
        $d = ' desc';
    } else { //si l'ordre croissant est choisi ou l'ordre n'est pas choisi alors ordre croissant par defaut
        $d = ' asc';
    }
    if ($gen != NULL) { //requete prepare avec les genres choisis
        $requete = $requete;
        $clause = implode(',', array_fill(0, count($gen), '?')); //pour ajouter plusieurs genres comme condition
        $stmt = $conn->prepare($requete . 'where nomGenre IN (' . $clause . ') order by ' . $tri . $d);
        $i = 1;
        foreach ($gen as $value) {
            $stmt->bindValue($i, $value);
            $i += 1;
        }
        $stmt->execute();
        $result = $stmt->fetchAll();
    } else { //requete normale si aucun genre a ete choisi
        $requete = $requete . 'order by ' . $tri;
        $result = $conn->query('SELECT * from film inner join realisateur ON FILM.idRealisateurFilm = realisateur.idRealisateur
    inner join genre ON FILM.idGenreFilm = genre.idGenre
    order by ' . $tri . $d);
    }
    return $result;
}

function affichageGenres(PDO $conn): void //affichage de checkbox avec tout les genres dans aside
{
    $result = $conn->query('SELECT * from genre');
    foreach ($result as $m) {
        echo '<input type="checkbox" id="genre' . $m['nomGenre'] . '" name="genre[' . $m['nomGenre'] . ']" value=' . $m['nomGenre'];
        if (isset($_GET['genre'])) {
            if (in_array($m['nomGenre'], $_GET['genre'])) {
                echo " checked >\n";
            }  //si le nom de genre du checkbox cree se trouve dans GET alors ce checkbox doit etre coche
            else {
                echo ">\n";
            }
        } else {
            echo ">\n";
        }
        echo '<label for="genre' . $m['nomGenre'] . '">' . $m['nomGenre'] . "</label>\n";
    }
}
