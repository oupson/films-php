<?php
require_once "imports.php";
$title = "Ajouter un film";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="./static/style/main.css" />
    <link rel="stylesheet" href="./static/style/addFilm.css" />
    <style>
        /* The Modal (background) */
        .modal {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Stay in place */
            z-index: 1;
            /* Sit on top */
            left: 0;
            top: 0;
            width: 100%;
            /* Full width */
            height: 100%;
            /* Full height */
            overflow: auto;
            /* Enable scroll if needed */
            background-color: rgb(0, 0, 0);
            /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4);
            /* Black w/ opacity */
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 15% auto;
            /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
            /* Could be more or less, depending on screen size */
        }

        /* The Close Button */
        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }
    </style>
</head>

<body>
    <?php include __ROOT__ . "/private/parts/header.php"; ?>
    <div class="article">
        <?php

        use FilmPHP\Database\Connexion;

        $conn = new Connexion();

        if (isLoggedIn($_COOKIE, $conn)) {
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                if (
                    isset($_POST["name_film"])
                    && isset($_POST["annee_film"])
                    && isset($_POST["id_realisateur_film"])
                    && isset($_POST["id_genre_film"])
                    && filter_var($_POST["annee_film"], FILTER_VALIDATE_INT, array("options" => array("min_range" => 1888, "max_range" => date("Y")))) !== false
                ) {
                    $imageUrl = filter_input(INPUT_POST, 'url_image_film', FILTER_VALIDATE_URL);

                    $st = $conn->prepare(
                        "INSERT INTO FILM (titreFilm, anneeFilm, imageFilm, idRealisateurFilm, idGenreFilm, descriptionFilm) VALUES(?, ?, ?, ?, ?, ?)"
                    );
                    $st->bindParam(1, $_POST["name_film"]);
                    $st->bindParam(2, $_POST["annee_film"]);
                    $st->bindParam(3, $imageUrl);
                    $st->bindParam(4, $_POST["id_realisateur_film"]);
                    $st->bindParam(5, $_POST["id_genre_film"]);
                    $st->bindParam(6, $_POST["descriptionFilm"]);
                    $st->execute();

                    echo "<div class=\"info-message\"><p>Inséré avec succès</p></div>\n";
                } else {
                    http_response_code(400);
                    echo "<div class=\"error-message\"><p>Il manque des informations</p></div>\n";
                }
            } else {
                include __ROOT__ . "/private/create/add_film_form.php";
            }
        } else {
            http_response_code(500);
            echo "<div class=\"error-message\"><p>Vous n'êtes pas connecté</p></div>";
        }
        ?>

    </div>
    <?php include __ROOT__ . "/private/parts/footer.php"; ?>
</body>

</html>