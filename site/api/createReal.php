<?php
require_once "../imports.php";

use FilmPHP\Database\Connexion;

header("Content-Type: application/json");

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $conn = new Connexion();
    if (isLoggedIn($_COOKIE, $conn)) {
        if (isset($_POST["nameReal"])) {
            $nameReal = $_POST["nameReal"];
            $imageReal = $_POST["imageReal"];
            $descReal = $_POST["descReal"];

            if ($conn->realExist($nameReal)) {
                http_response_code(500);
                echo json_encode(
                    [
                        "error" => [
                            "error_message" => "The real already exist",
                            "error_code" => "REAL_ALREADY_EXIST",
                        ],
                    ]
                );
            } else {
                $rowId = $conn->insertReal($nameReal, $imageReal, $descReal);
                echo json_encode(
                    [
                        "realId" => $rowId,
                        "nameReal" => $nameReal,
                        "imageReal" => $imageReal,
                        "descReal" => $descReal
                    ]
                );
            }
        } else {
            http_response_code(500);
            echo json_encode([
                "error" => [
                    "error_message" => "Missing parameters",
                    "error_code" => "MISSING_PARAMETERS",
                ],
            ]);
        }
    } else {
        http_response_code(403);
        echo json_encode([
            "error" => [
                "error_message" => "You don't have the right to do that",
                "error_code" => "UNAUTHORIZED",
            ],
        ]);
    }
} else {
    http_response_code(500);
    echo json_encode([
        "error" => [
            "error_message" => "Invalid method",
            "error_code" => "INVALID_METHOD",
        ],
    ]);
}
