<?php

declare(strict_types=1);

require_once "../imports.php";

use FilmPHP\Database\Connexion;

header("Content-Type: application/json");

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $conn = new Connexion();
    if (isLoggedIn($_COOKIE, $conn)) {
        if (isset($_POST["nameGenre"])) {
            $nameGenre = $_POST["nameGenre"];
            $descGenre = $_POST["descGenre"];

            if ($conn->genreExist($nameGenre)) {
                http_response_code(500);
                echo json_encode(
                    [
                        "error" => [
                            "error_message" => "The genre already exist",
                            "error_code" => "GENRE_ALREADY_EXIST",
                        ],
                    ]
                );
            } else {
                $rowId = $conn->insertGenre($nameGenre, $descGenre);
                echo json_encode(
                    [
                        "genreId" => $rowId,
                        "nameGenre" => $nameGenre,
                        "descGenre" => $descGenre
                    ]
                );
            }
        } else {
            http_response_code(500);
            echo json_encode([
                "error" => [
                    "error_message" => "Missing parameters",
                    "error_code" => "MISSING_PARAMETERS",
                ],
            ]);
        }
    } else {
        http_response_code(403);
        echo json_encode([
            "error" => [
                "error_message" => "You don't have the right to do that",
                "error_code" => "UNAUTHORIZED",
            ],
        ]);
    }
} else {
    http_response_code(500);
    echo json_encode([
        "error" => [
            "error_message" => "Invalid method",
            "error_code" => "INVALID_METHOD",
        ],
    ]);
}
