BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "UTILISATEUR" (
    "idUtilisateur" INTEGER NOT NULL,
    "nomUtilisateur" TEXT NOT NULL UNIQUE,
    "roleUtilisateur" NUMBER NOT NULL,
    "mdpUtilisateur" BLOB NOT NULL,
    "emailUtilisateur" TEXT NOT NULL UNIQUE,
    CHECK(
        roleUtilisateur >= 0
        AND roleUtilisateur < 4
    ),
    PRIMARY KEY("idUtilisateur")
);
CREATE TABLE IF NOT EXISTS "SESSION" (
    "tokenSession" TEXT NOT NULL,
    "idUtilisateur" NUMBER NOT NULL,
    FOREIGN KEY("idUtilisateur") REFERENCES "UTILISATEUR"("idUtilisateur"),
    PRIMARY KEY("tokenSession")
);
CREATE TABLE IF NOT EXISTS "GENRE" (
    "idGenre" INTEGER NOT NULL,
    "nomGenre" TEXT NOT NULL UNIQUE,
    "descriptionGenre" TEXT,
    PRIMARY KEY("idGenre")
);
CREATE TABLE IF NOT EXISTS "REALISATEUR" (
    "idRealisateur" INTEGER NOT NULL,
    "nomRealisateur" TEXT NOT NULL,
    "imageRealisateur" TEXT,
    "descriptionRealisateur" TEXT,
    PRIMARY KEY("idRealisateur")
);
CREATE TABLE IF NOT EXISTS "FILM" (
    "idFilm" INTEGER NOT NULL,
    "titreFilm" TEXT NOT NULL,
    "anneeFilm" INTEGER NOT NULL,
    "imageFilm" TEXT,
    "idRealisateurFilm" INTEGER NOT NULL,
    "idGenreFilm" INTEGER NOT NULL,
    "descriptionFilm" TEXT,
    FOREIGN KEY("idGenreFilm") REFERENCES "GENRE"("idGenre"),
    FOREIGN KEY("idRealisateurFilm") REFERENCES "REALISATEUR"("idRealisateur"),
    PRIMARY KEY("idFilm")
);
INSERT INTO "GENRE" ("idGenre", "nomGenre", "descriptionGenre")
VALUES (1, 'Drama', NULL);
INSERT INTO "GENRE" ("idGenre", "nomGenre", "descriptionGenre")
VALUES (2, 'Crime', NULL);
INSERT INTO "GENRE" ("idGenre", "nomGenre", "descriptionGenre")
VALUES (3, 'Action', NULL);
INSERT INTO "GENRE" ("idGenre", "nomGenre", "descriptionGenre")
VALUES (4, 'Biography', NULL);
INSERT INTO "GENRE" ("idGenre", "nomGenre", "descriptionGenre")
VALUES (5, 'Documentary', NULL);
INSERT INTO "REALISATEUR" (
        "idRealisateur",
        "nomRealisateur",
        "imageRealisateur",
        "descriptionRealisateur"
    )
VALUES (1, 'Frank Darabont', NULL, NULL);
INSERT INTO "REALISATEUR" (
        "idRealisateur",
        "nomRealisateur",
        "imageRealisateur",
        "descriptionRealisateur"
    )
VALUES (2, 'Francis Ford Coppola', NULL, NULL);
INSERT INTO "REALISATEUR" (
        "idRealisateur",
        "nomRealisateur",
        "imageRealisateur",
        "descriptionRealisateur"
    )
VALUES (3, 'Christopher Nolan', NULL, NULL);
INSERT INTO "REALISATEUR" (
        "idRealisateur",
        "nomRealisateur",
        "imageRealisateur",
        "descriptionRealisateur"
    )
VALUES (4, 'Sidney Lumet', NULL, NULL);
INSERT INTO "REALISATEUR" (
        "idRealisateur",
        "nomRealisateur",
        "imageRealisateur",
        "descriptionRealisateur"
    )
VALUES (5, 'Steven Spielberg', NULL, NULL);
INSERT INTO "REALISATEUR" (
        "idRealisateur",
        "nomRealisateur",
        "imageRealisateur",
        "descriptionRealisateur"
    )
VALUES (6, 'Peter Jackson', NULL, NULL);
INSERT INTO "REALISATEUR" (
        "idRealisateur",
        "nomRealisateur",
        "imageRealisateur",
        "descriptionRealisateur"
    )
VALUES (7, 'Quentin Tarantino', NULL, NULL);
INSERT INTO "REALISATEUR" (
        "idRealisateur",
        "nomRealisateur",
        "imageRealisateur",
        "descriptionRealisateur"
    )
VALUES (8, 'Michael Arick', NULL, NULL);
INSERT INTO "REALISATEUR" (
        "idRealisateur",
        "nomRealisateur",
        "imageRealisateur",
        "descriptionRealisateur"
    )
VALUES (9, 'David Fincher', NULL, NULL);
INSERT INTO "REALISATEUR" (
        "idRealisateur",
        "nomRealisateur",
        "imageRealisateur",
        "descriptionRealisateur"
    )
VALUES (10, 'Robert Zemeckis', NULL, NULL);
INSERT INTO "REALISATEUR" (
        "idRealisateur",
        "nomRealisateur",
        "imageRealisateur",
        "descriptionRealisateur"
    )
VALUES (11, 'Irvin Kershner', NULL, NULL);
INSERT INTO "REALISATEUR" (
        "idRealisateur",
        "nomRealisateur",
        "imageRealisateur",
        "descriptionRealisateur"
    )
VALUES (12, 'Lana Wachowski, Lilly Wachowski', NULL, NULL);
INSERT INTO "REALISATEUR" (
        "idRealisateur",
        "nomRealisateur",
        "imageRealisateur",
        "descriptionRealisateur"
    )
VALUES (13, 'Martin Scorsese', NULL, NULL);
INSERT INTO "REALISATEUR" (
        "idRealisateur",
        "nomRealisateur",
        "imageRealisateur",
        "descriptionRealisateur"
    )
VALUES (14, 'Milos Forman', NULL, NULL);
INSERT INTO "REALISATEUR" (
        "idRealisateur",
        "nomRealisateur",
        "imageRealisateur",
        "descriptionRealisateur"
    )
VALUES (15, 'Marty Gross', NULL, NULL);
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        1,
        'The Shawshank Redemption',
        1994,
        'https://m.media-amazon.com/images/M/MV5BMDFkYTc0MGEtZmNhMC00ZDIzLWFmNTEtODM1ZmRlYWMwMWFmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX128_CR0,3,128,176_AL_.jpg',
        1,
        1,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        2,
        'The Godfather',
        1972,
        'https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX128_CR0,1,128,176_AL_.jpg',
        2,
        2,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        3,
        'The Godfather: Part II',
        1974,
        'https://m.media-amazon.com/images/M/MV5BMWMwMGQzZTItY2JlNC00OWZiLWIyMDctNDk2ZDQ2YjRjMWQ0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX128_CR0,1,128,176_AL_.jpg',
        2,
        2,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        4,
        'The Dark Knight',
        2008,
        'https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_UX128_CR0,3,128,176_AL_.jpg',
        3,
        3,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        5,
        '12 Angry Men',
        1957,
        'https://m.media-amazon.com/images/M/MV5BMWU4N2FjNzYtNTVkNC00NzQ0LTg0MjAtYTJlMjFhNGUxZDFmXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_UX128_CR0,3,128,176_AL_.jpg',
        4,
        2,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        6,
        'Schindler''s List',
        1993,
        'https://m.media-amazon.com/images/M/MV5BNDE4OTMxMTctNmRhYy00NWE2LTg3YzItYTk3M2UwOTU5Njg4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX128_CR0,3,128,176_AL_.jpg',
        5,
        4,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        7,
        'The Lord of the Rings: The Return of the King',
        2003,
        'https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMTY3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX128_CR0,3,128,176_AL_.jpg',
        6,
        3,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        8,
        'Pulp Fiction',
        1994,
        'https://m.media-amazon.com/images/M/MV5BNGNhMDIzZTUtNTBlZi00MTRlLWFjM2ItYzViMjE3YzI5MjljXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX128_CR0,3,128,176_AL_.jpg',
        7,
        2,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        9,
        'The Good, the Bad and the Ugly',
        1966,
        'https://m.media-amazon.com/images/M/MV5BOTQ5NDI3MTI4MF5BMl5BanBnXkFtZTgwNDQ4ODE5MDE@._V1_UX128_CR0,3,128,176_AL_.jpg',
        8,
        5,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        10,
        'The Lord of the Rings: The Fellowship of the Ring',
        2001,
        'https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_UX128_CR0,3,128,176_AL_.jpg',
        6,
        3,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        11,
        'Fight Club',
        1999,
        'https://m.media-amazon.com/images/M/MV5BMmEzNTkxYjQtZTc0MC00YTVjLTg5ZTEtZWMwOWVlYzY0NWIwXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX128_CR0,3,128,176_AL_.jpg',
        9,
        1,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        12,
        'Forrest Gump',
        1994,
        'https://m.media-amazon.com/images/M/MV5BNWIwODRlZTUtY2U3ZS00Yzg1LWJhNzYtMmZiYmEyNmU1NjMzXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX128_CR0,3,128,176_AL_.jpg',
        10,
        1,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        13,
        'Inception',
        2010,
        'https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_UX128_CR0,3,128,176_AL_.jpg',
        3,
        3,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        14,
        'The Lord of the Rings: The Two Towers',
        2002,
        'https://m.media-amazon.com/images/M/MV5BZGMxZTdjZmYtMmE2Ni00ZTdkLWI5NTgtNjlmMjBiNzU2MmI5XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX128_CR0,3,128,176_AL_.jpg',
        6,
        3,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        15,
        'Star Wars: Episode V - The Empire Strikes Back',
        1980,
        'https://m.media-amazon.com/images/M/MV5BYmU1NDRjNDgtMzhiMi00NjZmLTg5NGItZDNiZjU5NTU4OTE0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX128_CR0,3,128,176_AL_.jpg',
        11,
        3,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        16,
        'The Matrix',
        1999,
        'https://m.media-amazon.com/images/M/MV5BNzQzOTk3OTAtNDQ0Zi00ZTVkLWI0MTEtMDllZjNkYzNjNTc4L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX128_CR0,3,128,176_AL_.jpg',
        12,
        3,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        17,
        'Goodfellas',
        1990,
        'https://m.media-amazon.com/images/M/MV5BY2NkZjEzMDgtN2RjYy00YzM1LWI4ZmQtMjIwYjFjNmI3ZGEwXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX128_CR0,3,128,176_AL_.jpg',
        13,
        4,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        18,
        'One Flew Over the Cuckoo''s Nest',
        1975,
        'https://m.media-amazon.com/images/M/MV5BZjA0OWVhOTAtYWQxNi00YzNhLWI4ZjYtNjFjZTEyYjJlNDVlL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX128_CR0,3,128,176_AL_.jpg',
        14,
        1,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        19,
        'Seven Samurai',
        1954,
        'https://m.media-amazon.com/images/M/MV5BOWE4ZDdhNmMtNzE5ZC00NzExLTlhNGMtY2ZhYjYzODEzODA1XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_UX128_CR0,1,128,176_AL_.jpg',
        15,
        5,
        NULL
    );
INSERT INTO "FILM" (
        "idFilm",
        "titreFilm",
        "anneeFilm",
        "imageFilm",
        "idRealisateurFilm",
        "idGenreFilm",
        "descriptionFilm"
    )
VALUES (
        20,
        'Se7en',
        1995,
        'https://m.media-amazon.com/images/M/MV5BOTUwODM5MTctZjczMi00OTk4LTg3NWUtNmVhMTAzNTNjYjcyXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX128_CR0,3,128,176_AL_.jpg',
        9,
        2,
        NULL
    );
COMMIT;