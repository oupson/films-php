CREATE TABLE FILM(
	idFilm INTEGER NOT NULL PRIMARY KEY,
	titreFilm TEXT NOT NULL,
	descriptionFilm TEXT,
	anneeFilm INTEGER NOT NULL,
	imageFilm TEXT,
	idRealisateurFilm INTEGER NOT NULL,
	idGenreFilm INTEGER NOT NULL,
	FOREIGN KEY(idRealisateurFilm) REFERENCES REALISATEUR(idRealisateur),
	FOREIGN KEY (idGenreFilm) REFERENCES GENRE(idGenre)
);

CREATE TABLE REALISATEUR(
	idRealisateur INTEGER NOT NULL PRIMARY KEY,
	nomRealisateur TEXT NOT NULL,
	descriptionRealisateur TEXT,
	imageRealisateur TEXT
);

CREATE TABLE GENRE(
	idGenre INTEGER NOT NULL PRIMARY KEY,
	nomGenre TEXT NOT NULL UNIQUE,
	descriptionGenre TEXT
);

CREATE TABLE UTILISATEUR(
	idUtilisateur INTEGER NOT NULL PRIMARY KEY,
	nomUtilisateur TEXT NOT NULL UNIQUE,
	-- 0 => utilisateur normal, 1 => modo, 2 => admin
	roleUtilisateur NUMBER NOT NULL,
	mdpUtilisateur BLOB NOT NULL,
	emailUtilisateur TEXT NOT NULL UNIQUE,
	CHECK(roleUtilisateur >= 0 AND roleUtilisateur < 4)
);

CREATE TABLE SESSION(
	tokenSession TEXT NOT NULL PRIMARY KEY,
	idUtilisateur NUMBER NOT NULL,
	FOREIGN KEY(idUtilisateur) REFERENCES UTILISATEUR(idUtilisateur)
);