package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	db, err := sql.Open("sqlite3", "film.db")
	if err != nil {
		log.Panic(err)
	}

	b, err := ioutil.ReadFile("../sql/create.sql")
	if err != nil {
		log.Panic(err)
	}
	sqlContent := string(b)

	_, err = db.Exec(sqlContent)
	if err != nil {
		log.Panic(err)
	}

	key := os.Getenv("IMDB_KEY")

	films, err := getFilms(key)
	if err != nil {
		log.Panic(err)
	}

	log.Printf("%+#v\n", films)

	for i := 0; i < 20; i++ {
		film := films.Items[i]
		fmt.Printf("\r%d", i)
		title, err := getFilm(key, film.Id)
		if err != nil {
			log.Panic(err)
		}

		if len(title.DirectorList) == 0 {

			continue
		}

		imageReal, err := getImages(key, title.DirectorList[0].Id)
		if err != nil {
			log.Panic(err)
		}

		st, err := db.Prepare("SELECT idRealisateur FROM REALISATEUR WHERE nomRealisateur = ?")
		if err != nil {
			log.Panic(err)
		}

		r := st.QueryRow(title.DirectorList[0].Name)
		var realId int64

		if r.Scan(&realId) == sql.ErrNoRows {
			log.Println()
			st, err := db.Prepare("REPLACE INTO REALISATEUR (nomRealisateur, imageRealisateur) VALUES(?, ?)")
			if err != nil {
				log.Panic(err)
			}

			r, err := st.Exec(title.DirectorList[0].Name, imageReal.getBest())
			if err != nil {
				log.Panic(err)
			}
			realId, err = r.LastInsertId()
			if err != nil {
				log.Panic(err)
			}
		}

		st, err = db.Prepare("SELECT idGenre FROM GENRE WHERE nomGenre = ?")
		if err != nil {
			log.Panic(err)
		}

		r = st.QueryRow(title.Genres)
		var genreId int64

		if r.Scan(&genreId) == sql.ErrNoRows {
			st, err := db.Prepare("REPLACE INTO GENRE (nomGenre) VALUES(?)")
			if err != nil {
				log.Panic(err)
			}

			r, err := st.Exec(title.Genres)
			if err != nil {
				log.Panic(err)
			}
			realId, err = r.LastInsertId()
			if err != nil {
				log.Panic(err)
			}
		}

		st, err = db.Prepare("INSERT INTO FILM (titreFilm, anneeFilm, imageFilm, idRealisateurFilm, idGenreFilm) VALUES(?, ?, ?, ?, ?)")
		if err != nil {
			log.Panic(err)
		}

		_, err = st.Exec(film.Title, film.Year, film.Image, realId, genreId)
		if err != nil {
			log.Panic(err)
		}
	}
}

type ImagesAnswer struct {
	Title string  `json:"title"`
	Items []Image `json:"image"`
}

func (im *ImagesAnswer) getBest() string {
	for _, i := range im.Items {
		if i.Title == im.Title {
			return i.Image
		}
	}

	return im.Items[0].Image
}

type Image struct {
	Title string `json:"title"`
	Image string `json:"image"`
}

type TitleAnswer struct {
	Genres       string     `json:"genres"`
	DirectorList []Director `json:"directorList"`
}

type Director struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type FilmsAnswer struct {
	Items []Film `json:"items"`
}

type Film struct {
	Id    string `json:"id"`
	Title string `json:"title"`
	Image string `json:"image"`
	Year  string `json:"year"`
}

func getFilms(key string) (*FilmsAnswer, error) {
	resp, err := http.Get(fmt.Sprintf("https://imdb-api.com/API/Top250Movies/%s", key))
	if err != nil {
		return nil, err
	}

	content, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, err
	}

	var res FilmsAnswer

	json.Unmarshal(content, &res)
	return &res, nil
}

func getFilm(key string, id string) (*TitleAnswer, error) {
	resp, err := http.Get(fmt.Sprintf("https://imdb-api.com/API/Title/%s/%s", key, id))
	if err != nil {
		return nil, err
	}

	content, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, err
	}

	var res TitleAnswer

	json.Unmarshal(content, &res)
	return &res, nil
}

func getImages(key string, id string) (*ImagesAnswer, error) {
	resp, err := http.Get(fmt.Sprintf("https://imdb-api.com/API/Title/%s/%s", key, id))
	if err != nil {
		return nil, err
	}

	content, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, err
	}

	var res ImagesAnswer

	json.Unmarshal(content, &res)
	return &res, nil
}
