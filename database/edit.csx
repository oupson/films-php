#r "nuget: System.Data.SQLite, 1.0.115"
#r "nuget: Newtonsoft.Json, 13.0.1"

using System;
using System.Net.Http;
using System.Data.SQLite;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

var apiKey = Args[0];

string cs = "Data Source=bak.db";

var conn = new SQLiteConnection(cs);
conn.Open();

var client = new HttpClient();


var cmd = new SQLiteCommand("SELECT idFilm, titreFilm FROM FILM", conn);

var filmCursor = cmd.ExecuteReader();



while (filmCursor.Read())
{
    Console.WriteLine($"Film: {filmCursor.GetString(1)}, Id : {filmCursor.GetInt64(0)}");
    var result = await client.GetStringAsync($"http://www.omdbapi.com/?t={filmCursor.GetString(1)}&apikey={apiKey}");
    var json = JsonConvert.DeserializeObject<JObject>(result);

    var genre = json["Genre"].ToObject<String>().Split(", ")[0];
    Console.WriteLine(genre);
    var director = json["Director"].ToObject<String>();


    var cmdDir = new SQLiteCommand(conn);
    cmdDir.CommandText = "SELECT idRealisateur FROM REALISATEUR WHERE nomRealisateur = @name";

    cmdDir.Parameters.AddWithValue("@name", director);

    cmdDir.Prepare();

    var directorId = cmdDir.ExecuteScalar();
    cmdDir.Dispose();
    if (directorId == null)
    {
        using (var cmdInsertDir = new SQLiteCommand(conn))
        {
            cmdInsertDir.CommandText = "REPLACE INTO REALISATEUR (nomRealisateur) VALUES(@name)";
            cmdInsertDir.Parameters.AddWithValue("@name", director);
            cmdInsertDir.Prepare();

            cmdInsertDir.ExecuteNonQuery();

            directorId = conn.LastInsertRowId;
        }
    }

    var cmd = new SQLiteCommand(conn);
    cmd.CommandText = "SELECT idGenre FROM GENRE WHERE nomGenre = @name";

    cmd.Parameters.AddWithValue("@name", genre);

    cmd.Prepare();

    var genreId = cmd.ExecuteScalar();
    cmd.Dispose();
    if (genreId == null)
    {
        using (var cmdGenreDir = new SQLiteCommand(conn))
        {
            cmdGenreDir.CommandText = "REPLACE INTO GENRE (nomGenre) VALUES(@name)";
            cmdGenreDir.Parameters.AddWithValue("@name", genre);
            cmdGenreDir.Prepare();

            cmdGenreDir.ExecuteNonQuery();

            genreId = conn.LastInsertRowId;
        }
    }

    cmd = new SQLiteCommand(conn);
    cmd.CommandText = "UPDATE FILM SET idRealisateurFilm = @idReal, idGenreFilm = @idGenre WHERE idFilm = @idFilm";
    cmd.Parameters.AddWithValue("@idReal", directorId);
    cmd.Parameters.AddWithValue("@idGenre", genreId);
    cmd.Parameters.AddWithValue("@idFilm", filmCursor.GetInt64(0));

    cmd.Prepare();
    cmd.ExecuteNonQuery();
    cmd.Dispose();
}

cmd.Dispose();

conn.Dispose();